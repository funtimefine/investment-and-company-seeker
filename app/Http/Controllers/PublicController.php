<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Project;

class PublicController extends Controller
{
    public function listInvestee()
    {
        $datas = Project::where('status', 2)->paginate(20);
        // return json_encode($datas);
        return view('home.index.list_project', compact('datas'));
    }
}
