<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Tag;

class TagController extends Controller
{
    public function index()
    {
        $datas = Tag::get();
        return view('admin.tag.index', compact('datas'));
    }

    public function add(Request $req)
    {
        $store = new Tag;
        $store->name = $req->tag;
        $store->save();

        return redirect()->route('tag');
    }

    public function delete($id='')
    {
        $datas = Tag::where('id', $id)->delete();
        return redirect()->route('tag');
    }
}
