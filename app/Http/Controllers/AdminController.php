<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.index');
    }

    public function listUser()
    {
    	$datas = User::where('role_id', '!=', 3)->get();
    	$page = '';
    	return view('admin.user.index', compact('datas','page'));
    }

    public function listUserStartup()
    {
    	$datas = User::where('role_id', 1)->get();
    	$page = '- Starup';
    	return view('admin.user.index', compact('datas', 'page'));
    }

    public function listUserInvestor()
    {
    	$datas = User::where('role_id', 2)->get();
    	$page = '- Investor';
    	return view('admin.user.index', compact('datas', 'page'));
    }
}
