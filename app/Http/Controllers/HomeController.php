<?php

namespace App\Http\Controllers;

use Auth;
use Storage;
use File;
use App\Model\Profil;
use App\Model\Tag;
use App\Model\Files;
use App\Model\TarckRecord;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()->role_id == 3){
            return redirect()->route('admin.index');
        }

        $data = Profil::where('user_id', Auth::user()->id)->first();
        return view('home.index.index', compact('data'));
    }

    public function edit()
    {
        $data = Profil::where('user_id', Auth::user()->id)->first();
        if(empty($data)){
            return view('home.index.add_profil');
        }
        return view('home.index.edit_profil', compact('data'));
    }

    public function add(Request $req)
    {
        $req->validate([
            'image' => 'required|image',
        ]);

        $image = $req->image;
        $filename = explode(' ', $req->name);
        $filename = 'logo-' . $filename[0] . '-' . time() . '.' . $image->getClientOriginalExtension();

        $image = Storage::putFileAs('logo/', $image, $filename);

        $data = new Profil;
        $data->user_id      = Auth::user()->id;
        $data->name         = $req->name;
        $data->email        = $req->email;
        $data->phone        = $req->phone;
        $data->address      = $req->address;
        $data->website      = $req->website;
        $data->year         = $req->year;
        $data->desc         = $req->desc;
        $data->image        = $filename;
        $data->save();

        return redirect()->route('home');
    }

    public function update(Request $req)
    {
        if ($req->image) {
            $req->validate([
                'image' => 'required|image',
            ]);

            $filename = Profil::find($req->id);
            $filename = $filename->image;

            $image = $req->image;
            $image = Storage::putFileAs('logo/', $image, $filename);

        }

        $data = Profil::find($req->id);
        $data->user_id      = Auth::user()->id;
        $data->name         = $req->name;
        $data->email        = $req->email;
        $data->phone        = $req->phone;
        $data->address      = $req->address;
        $data->website      = $req->website;
        $data->year         = $req->year;
        $data->desc         = $req->desc;
        $data->save();

        return redirect()->route('home');
    }

    public function getlogo($name = 'default.png')
    {
        $image = Storage::get('logo/'. $name);
        return $image;
    }

    public function getFile()
    {
        $data = Profil::where('user_id', Auth::user()->id)->first();
        $datas = Files::where('user_id', Auth::user()->id)->get();
        return view('home.file.index', compact(['datas', 'data']));
    }

    public function storeFile(Request $req)
    {
        $req->validate([
            'file'  => 'required',
            'name'  => 'required',
        ]);

        $filename = explode(' ', $req->name);
        $image = $req->file;
        $filename = 'file-' . $filename[0] . '-' . time() . '.' . $image->getClientOriginalExtension();
        $image = Storage::putFileAs('file/', $image, $filename);

        $store = new Files;
        $store->user_id     = Auth::user()->id;
        $store->name        = $req->name;
        $store->filename    = $filename;
        $store->save();

        return redirect()->route('file.get');
    }

    public function downloadFile($name='')
    {
        $file = Storage::download('file/'. $name);
        return $file;
    }

    public function deleteFile($id='')
    {
        $data = Files::find($id)->delete();
        return redirect()->route('file.get');
    }

    // S: Track Record

    public function getTrack()
    {
        $data = Profil::where('user_id', Auth::user()->id)->first();
        $datas = TarckRecord::where('user_id', Auth::user()->id)->get();

        $tag = Tag::get();

        return view('home.track.index', compact(['datas', 'data', 'tag']));
    }

    public function storeTrack(Request $req)
    {
        $req->validate([
            'file'  => 'required',
            'name'  => 'required',
        ]);

        $filename = explode(' ', $req->name);
        $image = $req->file;
        $filename = 'file-' . $filename[0] . '-' . time() . '.' . $image->getClientOriginalExtension();
        $image = Storage::putFileAs('trackrecord/', $image, $filename);

        $store = new TarckRecord;
        $store->user_id     = Auth::user()->id;
        $store->name        = $req->name;
        $store->filename    = $filename;
        $store->desc        = $req->desc;
        $store->bidang_id   = $req->bidang;
        $store->start       = $req->start;
        $store->end         = $req->end;
        $store->save();

        return redirect()->route('track.get');
    }

    public function downloadTrack($name='')
    {
        $file = Storage::download('trackrecord/'. $name);
        return $file;
    }

    public function deleteTrack($id='')
    {
        $data = Files::find($id)->delete();
        return redirect()->route('track.get');
    }

    // E: Track Record

    public function changePassword()
    {
        return view('home.changepassword');
    }

    public function savePassword(Request $req)
    {
        
    }

}
