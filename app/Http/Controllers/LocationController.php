<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Location;

class LocationController extends Controller
{
    public function index()
    {
        $datas = Location::get();
        return view('admin.location.index', compact('datas'));
    }

    public function add(Request $req)
    {
        $store = new Location;
        $store->name = $req->location;
        $store->save();

        return redirect()->route('location');
    }

    public function delete($id='')
    {
        Location::where('id', $id)->delete();
        return redirect()->route('location');
    }
}
