<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Project;
use Auth;

use App\Model\Tag;
use App\Model\Location;

class ProjectController extends Controller
{
    public function index()
    {
        Auth::user()->role_id == 1 ? $name='investee_id' : $name='investor_id';

        $tag = Tag::get();
        $lokasi = Location::get();

        $datas = Project::where($name, Auth::user()->id)->get();
        return view('home.project.index', compact('datas', 'tag', 'lokasi'));
    }

    public function store(Request $req)
    {
        $store = new Project;
        $store->name        = $req->name;
        $store->dana        = $req->dana;
        $store->desc        = $req->desc;
        $store->bidang_id   = $req->bidang;
        $store->lokasi_id   = $req->lokasi;
        $store->status      = 1;

        if(Auth::user()->role_id == 1){
            $store->investee_id = Auth::user()->id;
        }else{
            $store->investor_id = Auth::user()->id;
        }
        $store->save();

        return redirect()->route('project.index');
    }

    public function detail($id='')
    {
        $data = Project::find($id);
        dd($data);
    }

    public function publish($id = '')
    {
        $data = Project::find($id);
        $data->status  = 2;
        $data->save();

        return redirect()->route('project.index');
    }

    public function unpublish($id = '')
    {
        $data = Project::find($id);
        $data->status  = 1;
        $data->save();

        return redirect()->route('project.index');
    }

    public function edit($id = '')
    {
        $data = Project::find($id);
        dd($data);
    }

    public function delete($id='')
    {
        Project::find($id)->delete();
        return redirect()->route('project.index');
    }
}
