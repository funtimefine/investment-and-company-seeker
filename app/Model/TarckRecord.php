<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TarckRecord extends Model
{
    use SoftDeletes;
    protected $table = 'track_record';

    public function getTag()
    {
    	return $this->hasOne('App\Model\Tag', 'id', 'bidang_id');
    }
}
