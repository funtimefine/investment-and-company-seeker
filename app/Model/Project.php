<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;
    protected $table = 'project';

    public function getProfil()
    {
    	return $this->hasOne('App\Model\Profil', 'user_id', 'investee_id');
    }

    public function getBidang()
    {
    	return $this->hasOne('App\Model\Tag', 'id', 'bidang_id');
    }

    public function getLokasi()
    {
    	return $this->hasOne('App\Model\Location', 'id', 'lokasi_id');
    }
}
