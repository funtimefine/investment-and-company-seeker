<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('index.index');
})->name('index');

Auth::routes();

Route::get('list-investee', 'PublicController@listInvestee')->name('list.investee');

Route::middleware(['auth'])->group(function() {
    

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/edit', 'HomeController@edit')->name('edit');
    Route::post('/add', 'HomeController@add')->name('add');
    Route::post('/update', 'HomeController@update')->name('update');

    Route::get('/file', 'HomeController@getFile')->name('file.get');
    Route::post('/file', 'HomeController@storeFile')->name('file.store');
    Route::get('/file/{id}', 'HomeController@deleteFile')->name('file.delete');
    Route::get('/file/{name}/download', 'HomeController@downloadFile')->name('file.download');

    Route::get('/track-record', 'HomeController@getTrack')->name('track.get');
    Route::post('/track-record', 'HomeController@storeTrack')->name('track.store');
    Route::get('/track-record/{id}', 'HomeController@deleteTarck')->name('track.delete');
    Route::get('/track-record/{name}/download', 'HomeController@downloadTrack')->name('track.download');

    Route::get('project', 'ProjectController@index')->name('project.index');
    Route::post('project', 'ProjectController@store')->name('project.store');
    Route::get('project/{id}/detail', 'ProjectController@detail')->name('project.detail');
    Route::get('project/{id}/edit', 'ProjectController@edit')->name('project.edit');
    Route::post('project/{id}/update', 'ProjectController@update')->name('project.update');
    Route::get('project/{id}/delete', 'ProjectController@delete')->name('project.delete');
    Route::get('project/{id}/publish', 'ProjectController@publish')->name('project.publish');
    Route::get('project/{id}/unpublish', 'ProjectController@unpublish')->name('project.unpublish');

    Route::get('change-password', 'HomeController@changePassword')->name('changepassword');
    Route::post('save-password', 'HomeController@savePassword')->name('savepassword');


});

Route::middleware(['isadmin'])->group(function() {
    Route::get('/admin', 'AdminController@index')->name('admin.index');

    Route::get('/tag', 'TagController@index')->name('tag');
    Route::post('/tag', 'TagController@add')->name('tag.add');
    Route::get('/tag/{id}', 'TagController@delete')->name('tag.delete');

    Route::get('/location', 'LocationController@index')->name('location');
    Route::post('/location', 'LocationController@add')->name('location.add');
    Route::get('/location/{id}', 'LocationController@delete')->name('location.delete');

    Route::get('/users', 'AdminController@listUser')->name('getlist.user');
    Route::get('/users/startup', 'AdminController@listUserStartup')->name('getlist.startup');
    Route::get('/users/investor', 'AdminController@listUserInvestor')->name('getlist.investor');
});

Route::get('/logo/{name}', 'HomeController@getlogo')->name('get.logo');
