@extends('layouts.admin')
@section('admin')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">List User {{$page}}</h1>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card shadow">
            <div class="card-header">
                <h6>List User Terdaftar</h6>
            </div>
            <div class="card-body">
                <table class="table table-bordered dataTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $i = 1;
                        @endphp
                        @foreach($datas as $data)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>#{{Str::lower($data->name)}}</td>
                            <td>{{$data->email}}</td>
                            <td>{{$data->role_id == 1 ? 'Startup' : 'Investor' }}</td>
                            <td>
                                <a href="#" class="btn btn-primary">Lihat</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
