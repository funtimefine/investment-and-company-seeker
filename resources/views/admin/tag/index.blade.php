@extends('layouts.admin')
@section('admin')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Tag</h1>
</div>

<div class="row">
    <div class="col-lg-8">
        <div class="card shadow">
            <div class="card-header">
                <h6>Tag List</h6>
            </div>
            <div class="card-body">
                <table class="table table-bordered dataTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $i = 1;
                        @endphp
                        @foreach($datas as $data)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>#{{Str::lower($data->name)}}</td>
                            <td>
                                <a href="{{route('tag.delete', $data->id)}}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card shadow">
            <div class="card-header">
                <h6>Add Tag</h6>
            </div>
            <div class="card-body">
                <form class="" action="{{route('tag.add')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                      <input name="tag" type="text" class="form-control form-control-user" placeholder="Enter Tag Name...">
                    </div>
                    <button type="submit" class="btn btn-primary btn-user btn-block">
                          Save
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
