@extends('layouts.index')
@section('index')
<header>
  <nav class="navbar navbar-expand-xl absolute-nav transparent-nav cp-nav navbar-light bg-light fluid-nav">
    <a class="navbar-brand" href="index.html">
      <img src="{{asset('images/logo.png')}}" class="img-fluid" alt="">
    </a>
    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto main-nav">
        <li class="menu-item"><a title="Home" href="home-1.html">Home</a></li>
        <!-- <li class="menu-item dropdown">
          <a title="" href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true" aria-expanded="false">Investor</a>
          <ul  class="dropdown-menu">
            <li class="menu-item"><a href="job-listing.html">Investor Listing</a></li>
          </ul>
        </li> -->
        <li class="menu-item dropdown">
          <a title="" href="{{route('list.investee')}}" >Startup</a>
          <!-- <ul  class="dropdown-menu">
            <li class="menu-item"><a href="employer-listing.html">Startup Listing</a></li>
          </ul> -->
        </li>
        <li class="menu-item dropdown">
          <a title="" href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true" aria-expanded="false">Pages</a>
          <ul  class="dropdown-menu">
            <li class="menu-item"><a href="about-us.html">About Us</a></li>
            <li class="menu-item"><a href="{{route('login')}}">Login</a></li>
            <li class="menu-item"><a href="{{route('register')}}">Register</a></li>
          </ul>
        </li>
        @if (Auth::check())
        <li class="menu-item post-job"><a title="Title" href="{{route('home')}}">{{Auth::user()->name}}</a></li>
        @else
        <li class="menu-item post-job"><a title="Title" href="post-job.html"><i class="fas fa-plus"></i>Post a Job</a></li>
        @endif
      </ul>
    </div>
  </nav>
</header>
<!-- Header End -->

@yield('template')

<!-- Footer -->
<footer class="footer-bg">

  <div class="footer-widget-wrapper padding-bottom-60 padding-top-80">
    <div class="container">
      <div class="row">
        <div class="col-lg-5 col-sm-6">
          <div class="footer-widget widget-about">
            <h4>About Us</h4>
            <div class="widget-inner">
              <p class="description">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour.</p>
              <span class="about-contact"><i data-feather="phone-forwarded"></i>+8 246-345-0698</span>
              <span class="about-contact"><i data-feather="mail"></i>supportmail@gmail.com</span>
            </div>
          </div>
        </div>
        <div class="col-lg-2 offset-lg-1 col-sm-6">
          <div class="footer-widget footer-shortcut-link">
            <h4>Information</h4>
            <div class="widget-inner">
              <ul>
                <li><a href="#">About Us</a></li>
                <li><a href="#">Contact Us</a></li>
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Terms &amp; Conditions</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-sm-6">
          <div class="footer-widget footer-shortcut-link">
            <h4>Investor</h4>
            <div class="widget-inner">
              <ul>
                <li><a href="#">Create Account</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-sm-6">
          <div class="footer-widget footer-shortcut-link">
            <h4>Investee</h4>
            <div class="widget-inner">
              <ul>
                <li><a href="#">Create Account</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="footer-bottom-area">
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="footer-bottom border-top">
            <div class="row">
              <div class="col-xl-4 col-lg-5 order-lg-2">
                <div class="footer-app-download">
                  <a href="#" class="apple-app">Apple Store</a>
                  <a href="#" class="android-app">Google Play</a>
                </div>
              </div>
              <div class="col-xl-4 col-lg-4 order-lg-1">
                <p class="copyright-text">Copyright <a href="#">Oficiona</a> 2019, All right reserved</p>
              </div>
              <div class="col-xl-4 col-lg-3 order-lg-3">
                <div class="back-to-top">
                  <a href="#">Back to top<i class="fas fa-angle-up"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Footer End -->
@endsection
