@extends('index.template')
@section('template')

<!-- Banner -->
<div class="banner banner-1 banner-1-bg">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="banner-content">
          <h1>58,246 Company Listed</h1>
          <p>Create free account to find many Company, Startup & Project Opportunities around you!</p>
          <a href="{{route('register')}}" class="button">Join Us</a>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Banner End -->

<!-- Search and Filter -->
<div class="searchAndFilter-wrapper">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="searchAndFilter-block">
          <div class="searchAndFilter">
            <form action="#" class="search-form">
              <input type="text" placeholder="Enter Keywords">
              <select class="selectpicker" id="search-location">
                <option value="" selected>Location</option>
                <option value="california">California</option>
                <option value="las-vegas">Las Vegas</option>
                <option value="new-work">New Work</option>
                <option value="carolina">Carolina</option>
                <option value="chicago">Chicago</option>
                <option value="silicon-vally">Silicon Vally</option>
                <option value="washington">Washington DC</option>
                <option value="neveda">Neveda</option>
              </select>
              <select class="selectpicker" id="search-category">
                <option value="" selected>Category</option>
                <option value="real-state">Real State</option>
                <option value="vehicales">Vehicales</option>
                <option value="electronics">Electronics</option>
                <option value="beauty">Beauty</option>
                <option value="furnitures">Furnitures</option>
              </select>
              <button class="button primary-bg"><i class="fas fa-search"></i>Search Job</button>
            </form>
            <div class="filter-categories">
              <h4>Job Category</h4>
              <ul>
                <li><a href="job-listing.html"><i data-feather="bar-chart-2"></i>Accounting / Finance <span>(233)</span></a></li>
                <li><a href="job-listing.html"><i data-feather="edit-3"></i>Education <span>(46)</span></a></li>
                <li><a href="job-listing.html"><i data-feather="feather"></i>Design & Creative <span>(156)</span></a></li>
                <li><a href="job-listing.html"><i data-feather="briefcase"></i>Health Care <span>(98)</span></a></li>
                <li><a href="job-listing.html"><i data-feather="package"></i>Engineer & Architects <span>(188)</span></a></li>
                <li><a href="job-listing.html"><i data-feather="pie-chart"></i>Marketing & Sales <span>(124)</span></a></li>
                <li><a href="job-listing.html"><i data-feather="command"></i>Garments / Textile <span>(584)</span></a></li>
                <li><a href="job-listing.html"><i data-feather="globe"></i>Customer Support <span>(233)</span></a></li>
                <li><a href="job-listing.html"><i data-feather="headphones"></i>Digital Media <span>(15)</span></a></li>
                <li><a href="job-listing.html"><i data-feather="radio"></i>Telecommunication <span>(03)</span></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Search and Filter End -->

<!-- Registration Box -->
<div class="section-padding">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <div class="call-to-action-box candidate-box">
          <div class="icon">
            <img src="{{asset('images/index/investor.png')}}" alt="">
          </div>
          <span>Are You</span>
          <h3>Investor ?</h3>
          <a href="#" data-toggle="modal" data-target="#exampleModalLong2">Register Now <i class="fas fa-arrow-right"></i></a>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="call-to-action-box employer-register">
          <div class="icon">
            <img src="{{asset('images/index/startup.png')}}" alt="">
          </div>
          <span>Are You</span>
          <h3>Investee ?</h3>
          <a href="#" data-toggle="modal" data-target="#exampleModalLong3">Register Now <i class="fas fa-arrow-right"></i></a>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Registration Box End -->

<!-- Modal -->
<div class="account-entry">
  <div class="modal fade" id="exampleModalLong3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"><i data-feather="edit"></i>Registration</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="account-type">
            <a href="#" class="candidate-acc"><i data-feather="user"></i>Candidate</a>
            <a href="#" class="employer-acc active"><i data-feather="briefcase"></i>Employer</a>
          </div>
          <form action="#">
            <div class="form-group">
              <input type="text" placeholder="Username" class="form-control">
            </div>
            <div class="form-group">
              <input type="email" placeholder="Email Address" class="form-control">
            </div>
            <div class="form-group">
              <input type="password" placeholder="Password" class="form-control">
            </div>
            <div class="more-option terms">
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck3">
                <label class="form-check-label" for="defaultCheck3">
                  I accept the <a href="#">terms & conditions</a>
                </label>
              </div>
            </div>
            <button class="button primary-bg btn-block">Register</button>
          </form>
          <div class="shortcut-login">
            <span>Or connect with</span>
            <div class="buttons">
              <a href="#" class="facebook"><i class="fab fa-facebook-f"></i>Facebook</a>
              <a href="#" class="google"><i class="fab fa-google"></i>Google</a>
            </div>
            <p>Already have an account? <a href="{{route('login')}}">Login</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
