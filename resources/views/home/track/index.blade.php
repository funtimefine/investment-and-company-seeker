@extends('layouts.index')
@section('index')

<header class="header-2">
      <div class="container">
        <div class="row">
          <div class="col">

            @include('home.block.navbar')

          </div>
        </div>
      </div>
    </header>

    <!-- Breadcrumb -->
    <div class="alice-bg padding-top-70 padding-bottom-70">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="breadcrumb-area">
              <h1>Track Record Perusahaan</h1>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Breadcrumb End -->

    <div class="alice-bg section-padding-bottom">
      <div class="container no-gliters">
        <div class="row no-gliters">
          <div class="col">
            <div class="dashboard-container">
              <div class="dashboard-content-wrapper">
                <div class="dashboard-section user-statistic-block">
                    <div align="center">
                        <button type="button" class="btn btn-lg btn-primary" data-toggle="modal" data-target="#myModal">Add New File</button>
                    </div>
                    <!-- The Modal -->
                    <div class="modal" id="myModal">
                      <div class="modal-dialog">
                        <div class="modal-content">

                          <!-- Modal Header -->
                          <div class="modal-header">
                            <h4 class="modal-title">Modal Heading</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                          </div>

                          <form action="{{route('track.store')}}" method="post" enctype="multipart/form-data">
                              {{csrf_field()}}

                              <!-- Modal body -->
                              <div class="modal-body">
                                  <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Name Project</label>
                                    <div class="col-sm-9">
                                      <input type="text" class="form-control" name="name" placeholder="" required>
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Deskripsi Project</label>
                                    <div class="col-sm-9">
                                      <input type="text" class="form-control" name="desc" placeholder="" required>
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Select Bidang</label>
                                    <div class="col-sm-9">
                                      <select class="browser-default custom-select" name="bidang">
                                        <option selected>Open this select menu</option>
                                        @foreach($tag as $bidang)
                                        <option value="{{$bidang->id}}">{{$bidang->name}}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Start Project</label>
                                    <div class="col-sm-9">
                                      <input type="date" class="form-control" name="start" placeholder="" required>
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">End Project</label>
                                    <div class="col-sm-9">
                                      <input type="date" class="form-control" name="end" placeholder="" required>
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">File Upload</label>
                                    <div class="col-sm-9">
                                        <div class="custom-file">
                                          <input type="file" class="custom-file-input" name="file">
                                          <label class="custom-file-label" >Choose file</label>
                                        </div>
                                    </div>
                                  </div>

                                  <div class="input-group">

                                  </div>
                              </div>

                              <!-- Modal footer -->
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                              </div>

                          </form>

                        </div>
                      </div>
                    </div>
                    <!-- End -->
                </div>
                <div class="dashboard-section user-statistic-block">
                    <table class="table border">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Desc</th>
                                <th>Bidang</th>
                                <th>Project</th>
                                <th>File</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $i = 1;
                            @endphp
                            @foreach($datas as $data)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$data->name}}</td>
                                <td>{{$data->desc}}</td>
                                <td>{{$data->getTag->name}}</td>
                                <td>{{$data->start}} - {{$data->end}}</td>
                                <td><a href="{{route('track.download', $data->filename )}}" class="btn btn-success">Download</a></td>
                                <td><a href="{{route('track.delete', $data->id)}}" onclick="return confirm('Are you sure you want to delete this item?')" class="btn btn-primary">Delete</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
              </div>

              <div class="dashboard-sidebar">

                @include('home.block.sidebar')

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection
