@extends('layouts.index')
@section('index')

<header class="header-2">
      <div class="container">
        <div class="row">
          <div class="col">

            @include('home.block.navbar')

          </div>
        </div>
      </div>
    </header>

    <!-- Breadcrumb -->
    <div class="alice-bg padding-top-70 padding-bottom-70">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="breadcrumb-area">
              <h1>Edit Profil</h1>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Breadcrumb End -->

    <div class="alice-bg section-padding-bottom">
      <div class="container no-gliters">
        <div class="row no-gliters">
          <div class="col">
              @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
            <div class="dashboard-container">
              <div class="dashboard-content-wrapper">
                <!-- Start main content -->
                <form action="{{route('update')}}" method="post" enctype="multipart/form-data" class="dashboard-form">
                  {{csrf_field()}}
                  <input type="hidden" name="id" value="{{$data->id}}">
                  <div class="dashboard-section upload-profile-photo">
                    <div class="update-photo">
                      <img class="image" src="{{route('get.logo', ($data->image ? $data->image : 'default.png'))}}" alt="" >
                    </div>
                    <div class="file-upload">
                      <input type="file" class="file-input" name="image">Change Profile
                    </div>
                  </div>
                  <div class="dashboard-section basic-info-input">
                    <h4><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user-check"><path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle><polyline points="17 11 19 13 23 9"></polyline></svg>Basic Info</h4>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Company Name</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" name="name" value="{{$data->name}}" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Email Address</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" name="email" value="{{$data->email}}" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Phone</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" name="phone" value="{{$data->phone}}" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Address</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" name="address" value="{{$data->address}}" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Website</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" name="website" value="{{$data->website}}" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Year Company</label>
                      <div class="col-sm-9">
                        <input type="number" class="form-control" name="year" value="{{$data->year}}" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">About</label>
                      <div class="col-sm-9">
                        <textarea class="form-control" name="desc" required>{{$data->desc}}</textarea>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label"></label>
                      <div class="col-sm-9">
                        <button type="submit" class="button">Save Change</button>
                      </div>
                    </div>
                  </div>
                </form>

                <!-- <div class="dashboard-section basic-info-input">
                  <h4><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-lock"><rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect><path d="M7 11V7a5 5 0 0 1 10 0v4"></path></svg>Change Password</h4>
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Current Password</label>
                    <div class="col-sm-9">
                      <input type="password" class="form-control" placeholder="Current Password">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">New Password</label>
                    <div class="col-sm-9">
                      <input type="password" class="form-control" placeholder="New Password">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Retype Password</label>
                    <div class="col-sm-9">
                      <input type="password" class="form-control" placeholder="Retype Password">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label"></label>
                    <div class="col-sm-9">
                      <button class="button">Save Change</button>
                    </div>
                  </div>
                </div> -->
                <!-- end main content -->
              </div>
              <div class="dashboard-sidebar">

                @include('home.block.sidebar')

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


@endsection
