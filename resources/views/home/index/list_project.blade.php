@extends('layouts.index')
@section('index')

<div class="job-listing-container">
  <div class="filtered-job-listing-wrapper">
    <div class="job-view-controller-wrapper">
      <div class="job-view-controller">
        <div class="controller list active">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
        </div>
        <div class="controller grid">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-grid"><rect x="3" y="3" width="7" height="7"></rect><rect x="14" y="3" width="7" height="7"></rect><rect x="14" y="14" width="7" height="7"></rect><rect x="3" y="14" width="7" height="7"></rect></svg>
        </div>
        
      </div>
     
    </div>

    <!-- S:content -->
    <div class="job-filter-result">
       @foreach($datas as $data)
      <div class="job-list">
        <div class="thumb">
          <a href="#">
            <img src="{{route('get.logo', $data->getProfil->image)}}" height="100%" width="100%"  alt="">
          </a>
        </div>
        <div class="body">
          <div class="content">
            <h4><a href="">{{$data->name}}</a></h4>
            <div class="info">

              <span class="company">
                <a href="#">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-briefcase"><rect x="2" y="7" width="20" height="14" rx="2" ry="2"></rect><path d="M16 21V5a2 2 0 0 0-2-2h-4a2 2 0 0 0-2 2v16"></path></svg>
                  Theoreo
                </a>
              </span>

              <span class="office-location">
                <a href="#">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-map-pin"><path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z"></path><circle cx="12" cy="10" r="3"></circle></svg>
                  New Orleans
                 </a>
              </span>
              
              <span class="job-type temporary">
                <a href="#">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-clock"><circle cx="12" cy="12" r="10"></circle><polyline points="12 6 12 12 16 14"></polyline></svg>
                  Temporary
                </a>
              </span>

            </div>
          </div>

          <div class="more">
            <div class="buttons">
              <a href="#" class="button" data-toggle="modal" data-target="#apply-popup-id">Apply Now</a>
            </div>  
          </div>

        </div>
      </div>
      @endforeach
    </div>
    <!-- e:content -->

    <!-- s:pagination -->
    <div class="pagination-list text-center">
      <nav class="navigation pagination">
        <div class="nav-links">
          <a class="prev page-numbers" href="#"><i class="fas fa-angle-left"></i></a>
          <a class="page-numbers" href="#">1</a>
          <span aria-current="page" class="page-numbers current">2</span>
          <a class="page-numbers" href="#">3</a>
          <a class="page-numbers" href="#">4</a>
          <a class="next page-numbers" href="#"><i class="fas fa-angle-right"></i></a>
        </div>
      </nav>
    </div>
  </div>
  <!-- E:pagination -->

  <div class="job-filter-wrapper">
    
    
    
    <!-- S:sidebar -->
    <div class="job-filter">
      <h4>Menu</h4>
      <ul>
        <li><a href="{{route('index')}}" target="_blank">Home</a></li>
        <li><a href="{{route('home')}}" >Dashboard</a></li>
      </ul>
    </div>

    <!-- <div data-id="gender" class="job-filter same-pad gender">
      <h4 class="option-title">Gender</h4>
      <ul>
        <li><a href="#" data-attr="Male">Male</a></li>
        <li><a href="#" data-attr="Female">Female</a></li>
      </ul>
    </div> -->
    <!-- E:sidebar -->
    
  </div>
</div>

@endsection
