@extends('layouts.index')
@section('index')

<header class="header-2">
      <div class="container">
        <div class="row">
          <div class="col">

            @include('home.block.navbar')

          </div>
        </div>
      </div>
    </header>

    <!-- Breadcrumb -->
    <div class="alice-bg padding-top-70 padding-bottom-70">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="breadcrumb-area">
              <h1>Dashboard</h1>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Breadcrumb End -->

    <div class="alice-bg section-padding-bottom">
      <div class="container no-gliters">
        <div class="row no-gliters">
          <div class="col">
            <div class="dashboard-container">
              <div class="dashboard-content-wrapper">
                <div class="dashboard-section user-statistic-block">
                  <div class="user-statistic">
                    <i data-feather="pie-chart"></i>
                    <h3>132</h3>
                    <span>Companies Viewed</span>
                  </div>
                  <div class="user-statistic">
                    <i data-feather="briefcase"></i>
                    <h3>12</h3>
                    <span>Applied Jobs</span>
                  </div>
                  <div class="user-statistic">
                    <i data-feather="heart"></i>
                    <h3>32</h3>
                    <span>Favourite Jobs</span>
                  </div>
                </div>

                @if($data)
                <div class="dashboard-section user-statistic-block">
                    <div class="information-and-contact">
                    <div class="information">
                      <h4>Profil</h4>
                      <ul>
                        <li><span>Name:</span> {{$data->name}}</li>
                        <li><span>Role:</span> {{Auth::user()->role_id == 1 ? 'Investee' : 'Investor'}}</li>
                        <li><span>Website:</span> {{$data->website}} </li>
                        <li><span>Phone:</span> {{$data->phone}}</li>
                        <li><span>Email:</span> {{$data->email}}</li>
                        <li><span>Tahun Berdiri:</span> {{$data->year}}</li>
                        <li><span>Address:</span> {{$data->address}}</li>
                        <li><span>About:</span> {{$data->desc}}</li>
                      </ul>
                    </div>
                  </div>
                </div>
                @endif
              </div>

              <div class="dashboard-sidebar">

                @include('home.block.sidebar')

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


@endsection
