@extends('layouts.index')
@section('index')

<header class="header-2">
      <div class="container">
        <div class="row">
          <div class="col">

            @include('home.block.navbar')

          </div>
        </div>
      </div>
    </header>

    <!-- Breadcrumb -->
    <div class="alice-bg padding-top-70 padding-bottom-70">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="breadcrumb-area">
              <h1>Add Profil</h1>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Breadcrumb End -->

    <div class="alice-bg section-padding-bottom">
      <div class="container no-gliters">
        <div class="row no-gliters">
          <div class="col">
              @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
            <div class="dashboard-container">
              <div class="dashboard-content-wrapper">
                <!-- Start main content -->
                <form action="{{route('add')}}" method="post" class="dashboard-form" enctype="multipart/form-data">
                  {{csrf_field()}}
                  <div class="dashboard-section upload-profile-photo">
                    <div class="update-photo">
                      <img class="image" src="#" alt="" >
                    </div>
                    <div class="file-upload">
                      <input type="file" name="image" class="file-input" required>Add Profile
                    </div>
                  </div>
                  <div class="dashboard-section basic-info-input">
                    <h4><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user-check"><path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle><polyline points="17 11 19 13 23 9"></polyline></svg>Basic Info</h4>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Company Name</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" name="name" placeholder="Name" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Email Address</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" name="email" placeholder="email@example.com" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Phone</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" name="phone" placeholder="+55 123 4563 4643" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Address</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" name="address" placeholder="Washington D.C" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Website</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" name="website" placeholder="www.example.com" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Year Company</label>
                      <div class="col-sm-9">
                        <input type="number" class="form-control" name="year" placeholder="2019" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">About</label>
                      <div class="col-sm-9">
                        <textarea class="form-control" name="desc" placeholder="Introduce Yourself" required></textarea>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label"></label>
                      <div class="col-sm-9">
                        <button type="submit" class="button">Save Change</button>
                      </div>
                    </div>
                  </div>
                </form>
                <!-- end main content -->
              </div>
              <div class="dashboard-sidebar">

                @include('home.block.sidebar')

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


@endsection
