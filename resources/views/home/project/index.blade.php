@extends('layouts.index')
@section('index')

<header class="header-2">
      <div class="container">
        <div class="row">
          <div class="col">

            @include('home.block.navbar')

          </div>
        </div>
      </div>
    </header>

    <!-- Breadcrumb -->
    <div class="alice-bg padding-top-70 padding-bottom-70">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="breadcrumb-area">
              <h1>Project</h1>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Breadcrumb End -->

    <div class="alice-bg section-padding-bottom">
      <div class="container no-gliters">
        <div class="row no-gliters">
          <div class="col">
            <div class="dashboard-container">
              <div class="dashboard-content-wrapper">
                <div class="dashboard-section user-statistic-block">
                    @if(Auth::user()->role_id == 1)
                    <div align="center">
                        <button type="button" class="btn btn-lg btn-primary" data-toggle="modal" data-target="#myModal">Add New Project</button>
                    </div>
                    <!-- The Modal -->
                    <div class="modal" id="myModal">
                      <div class="modal-dialog">
                        <div class="modal-content">

                          <!-- Modal Header -->
                          <div class="modal-header">
                            <h4 class="modal-title">Tambah Project</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                          </div>

                          <form action="{{route('project.store')}}" method="post" enctype="multipart/form-data">
                              {{csrf_field()}}

                              <!-- Modal body -->
                              <div class="modal-body">
                                  <div class="form-group">
                                    <input type="text" placeholder="Name Project" class="form-control" name="name">
                                  </div>
                                  <div class="form-group">
                                    <input type="number" placeholder="Dana Project" class="form-control" name="dana">
                                  </div>
                                  <div class="form-group">
                                    <input type="text" placeholder="Deskripsi Project" class="form-control" name="desc">
                                  </div>

                                  <div class="form-group ">
                                    <select class="browser-default custom-select" name="bidang">
                                        <option selected>Open this select menu</option>
                                        @foreach($tag as $bidang)
                                        <option value="{{$bidang->id}}">{{$bidang->name}}</option>
                                        @endforeach
                                    </select>
                                  </div>

                                  <div class="form-group ">
                                    <select class="browser-default custom-select" name="lokasi">
                                        <option selected>Open this select menu</option>
                                        @foreach($lokasi as $loc)
                                        <option value="{{$loc->id}}">{{$loc->name}}</option>
                                        @endforeach
                                    </select>
                                  </div>

                              </div>

                              <!-- Modal footer -->
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                              </div>

                          </form>

                        </div>
                      </div>
                    </div>
                    <!-- End -->
                    @endif
                </div>
                <div class="dashboard-section user-statistic-block">
                    <table class="table border">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Dana</th>
                                <th>Bidang</th>
                                <th>Lokasi</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $i = 1;
                            @endphp
                            @foreach($datas as $data)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$data->name}}</td>
                                <td>Rp. {{ number_format($data->dana, 2)}}</td>
                                <td>{{$data->getBidang ? $data->getBidang->name : ''}}</td>
                                <td>{{$data->getLokasi ? $data->getLokasi->name : ''}}</td>
                                <td><span class="btn btn-success">{{ $data->status == 1 ? 'Pending' : ($data->status == 2 ? 'Publish' : ($data->status == 3 ? 'Have Investor' : 'Done'))}}</span></td>
                                <td>
                                    <a href="{{route('project.detail', $data->id)}}" class="btn btn-success">detail</a>
                                    
                                    @if($data->status == 1)
                                    <a href="{{route('project.publish', $data->id)}}" onclick="return confirm('Are you sure you want to publish this item?')" class="btn btn-warning">Publish</a>
                                    <a href="{{route('project.delete', $data->id)}}" onclick="return confirm('Are you sure you want to delete this item?')" class="btn btn-primary">Delete</a>
                                    @else
                                    <a href="{{route('project.unpublish', $data->id)}}" onclick="return confirm('Are you sure you want to publish this item?')" class="btn btn-danger">Un Publish</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
              </div>

              <div class="dashboard-sidebar">

                @include('home.block.sidebar')

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection
