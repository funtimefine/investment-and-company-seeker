
  <div class="user-info">
    <div class="thumb">
      <img src="{{route('get.logo', ( empty($data->image) ? 'default.png' : $data->image))}}" class="img-fluid" alt="">
    </div>
    <div class="user-body">
      <h5>{{Auth::user()->name}}</h5>
      <span>{{Auth::user()->email}}</span>
    </div>
  </div>

<div class="dashboard-menu">
  <ul>
    <li class=""><i class="fas fa-home"></i><a href="{{route('home')}}">Dashboard</a></li>
    <li><i class="fas fa-user"></i><a href="{{route('edit')}}">Edit Profile</a></li>
    <li><i class="fas fa-file-alt"></i><a href="{{route('file.get')}}">File Pendukung</a></li>
    <hr>
    @if(Auth::user()->role_id == 1)
    <li><i class="fas fa-check-square"></i><a href="{{route('project.index')}}">Project</a></li>
    @else
    <li><i class="fas fa-check-square"></i><a href="{{route('project.index')}}">Project</a></li>
    <!-- <li><i class="fas fa-edit"></i><a href="edit-resume.html">Edit Resume</a></li>
    <li><i class="fas fa-heart"></i><a href="dashboard-bookmark.html">Bookmarked</a></li>
    <li><i class="fas fa-check-square"></i><a href="dashboard-applied.html">Applied Job</a></li>
    <li><i class="fas fa-comment"></i><a href="dashboard-message.html">Message</a></li>
    <li><i class="fas fa-calculator"></i><a href="dashboard-pricing.html">Pricing Plans</a></li> -->
    @endif
    <li><i class="fas fa-calculator"></i><a href="{{route('track.get')}}">Track-Record Experience</a></li>
  </ul>
  <ul class="delete">
    <li class="#">
        <i class="fas fa-power-off"></i>
        <a href="{{route('changepassword')}}">Change Password</a>
    </li>
    <li>
        <i class="fas fa-power-off"></i><a href="{{ route('logout') }}" onclick="event.preventDefault();
        document.getElementById('logout-form').submit();">Logout</a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </li>
    <!-- <li><i class="fas fa-trash-alt"></i><a href="#" data-toggle="modal" data-target="#modal-delete">Delete Profile</a></li> -->
  </ul>
  <!-- Modal -->
  <div class="modal fade modal-delete" id="modal-delete" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <h4><i data-feather="trash-2"></i>Delete Account</h4>
          <p>Are you sure! You want to delete your profile. This can't be undone!</p>
          <form action="#">
            <div class="form-group">
              <input type="password" class="form-control" placeholder="Enter password">
            </div>
            <div class="buttons">
              <button class="delete-button">Save Update</button>
              <button class="">Cancel</button>
            </div>
            <div class="form-group form-check">
              <input type="checkbox" class="form-check-input" checked="">
              <label class="form-check-label">You accepts our <a href="#">Terms and Conditions</a> and <a href="#">Privacy Policy</a></label>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
