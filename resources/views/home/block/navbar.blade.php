<div class="header-top">
  <div class="logo-area">
    <a href="index.html"><img src="#" alt="Tidak Ada Gambar"></a>
  </div>
</div>


<nav class="navbar navbar-expand-lg cp-nav-2">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav">
      <li class="menu-item active"><a title="Home" href="{{url('/')}}">Home</a></li>
      <li class="menu-item post-job"><a href="{{route('home')}}"><i class="fas fa-plus"></i>{{Auth::check() ? Auth::user()->name : 'Login'}}</a></li>
    </ul>
  </div>
</nav>
